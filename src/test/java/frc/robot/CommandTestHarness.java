package frc.robot;

import edu.wpi.first.wpilibj2.command.CommandScheduler;

import org.junit.After;
import org.junit.Before;

/**
 * Resets the command scheduler before every test.  All command tests should subclass this.
 */
public class CommandTestHarness extends HardwareTestHarness {
  @Before
  public void enableScheduler() {
    CommandScheduler.getInstance().enable();
    CommandScheduler.getInstance().cancelAll();
    CommandScheduler.getInstance().clearButtons();
  }

  @After
  public void disableScheduler() {
    CommandScheduler.getInstance().disable();
    CommandScheduler.getInstance().cancelAll();
    CommandScheduler.getInstance().clearButtons();
  }
}
