package frc.robot;

import org.junit.Test;

import static org.junit.Assert.*;

public class RgbToHslTest {

  @Test
  public void testBlackInputs() {
    var rgb = new double[]{0, 0, 0};
    var hsl = new double[3];

    RgbToHsl.convert(rgb, hsl, 255);

    assertArrayEquals(new double[]{0, 0, 0}, hsl, 0);
  }

  @Test
  public void testWhiteInputsWithNormalizationFactor255() {
    int maxValue = 255;
    var rgb = new double[]{maxValue, maxValue, maxValue};
    var hsl = new double[3];

    RgbToHsl.convert(rgb, hsl, maxValue);

    assertArrayEquals(new double[]{0, 0, 1.0}, hsl, 0);
  }

  @Test
  public void testWhiteInputsWithNormalizationFactor65535() {
    int maxValue = 65535;
    var rgb = new double[]{maxValue, maxValue, maxValue};
    var hsl = new double[3];

    RgbToHsl.convert(rgb, hsl, maxValue);

    assertArrayEquals(new double[]{0, 0, 1.0}, hsl, 0);
  }

  @Test
  public void testRed() {
    var rgb = new double[]{1, 0, 0};
    var hsl = new double[3];

    RgbToHsl.convert(rgb, hsl, 1);

    assertArrayEquals(new double[]{0, 1, 0.5}, hsl, 0);
  }

  @Test
  public void testGreen() {
    var rgb = new double[]{0, 1, 0};
    var hsl = new double[3];

    RgbToHsl.convert(rgb, hsl, 1);

    assertArrayEquals(new double[]{120, 1, 0.5}, hsl, 0);
  }

  @Test
  public void testBlue() {
    var rgb = new double[]{0, 0, 1};
    var hsl = new double[3];

    RgbToHsl.convert(rgb, hsl, 1);

    assertArrayEquals(new double[]{240, 1, 0.5}, hsl, 0);
  }

  @Test
  public void testNegativeHue() {
    var rgb = new double[]{3, 0, 1}; // Hue should be +358, not -2
    var hsl = new double[3];

    RgbToHsl.convert(rgb, hsl, 255);

    assertArrayEquals(new double[]{358, 1, 0.00588}, hsl, 0.0001);
  }
}
