package frc.robot;

import org.junit.Test;

public class RobotSanityTests extends CommandTestHarness {
  private final Robot robot = new Robot();

  // Note: if an error is thrown, the test will automatically fail

  @Test
  @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
  public void testDoesNotErrorOnStart() {
    robot.robotInit();
  }

  @Test
  @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
  public void testDoesNotErrorInAuton() {
    robot.robotInit();

    robot.autonomousInit();

    for (int i = 0; i < 10; i++) {
      robot.autonomousPeriodic();
      robot.robotPeriodic();
    }
  }

  @Test
  @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
  public void testDoesNotErrorInTeleop() {
    robot.robotInit();

    robot.autonomousInit();

    for (int i = 0; i < 10; i++) {
      robot.autonomousPeriodic();
      robot.robotPeriodic();
    }

    for (int i = 0; i < 20; i++) {
      robot.teleopInit();
      robot.teleopPeriodic();
      robot.robotPeriodic();
    }
  }
}
