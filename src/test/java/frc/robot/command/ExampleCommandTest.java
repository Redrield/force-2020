package frc.robot.command;

import frc.robot.CommandTestHarness;
import frc.robot.commands.ExampleCommand;
import frc.robot.subsystems.ExampleSubsystem;

import java.util.Set;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example tests for a command.
 */
public class ExampleCommandTest extends CommandTestHarness {
  @Test
  public void testRequires() {
    ExampleSubsystem subsystem = new ExampleSubsystem();
    ExampleCommand command = new ExampleCommand(subsystem);

    assertEquals(
        "Command should require the example subsystem",
        Set.of(subsystem),
        command.getRequirements()
    );
  }
}
