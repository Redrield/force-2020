
package frc.robot.commands;

import frc.robot.subsystems.Hopper;

import edu.wpi.first.wpilibj2.command.CommandBase;

public class ActuateHopperMotor extends CommandBase {
  private final Hopper hopper;
  private final double speed;

  public ActuateHopperMotor(Hopper hopper, double speed) {
    this.hopper = hopper;
    this.speed = speed;
    addRequirements(hopper);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    hopper.setHopperRaw(speed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    hopper.setHopperRaw(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
