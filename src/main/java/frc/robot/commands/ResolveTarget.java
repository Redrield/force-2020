
package frc.robot.commands;

import frc.robot.subsystems.Turret;

import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.CommandBase;

import java.util.function.DoubleSupplier;

import static frc.robot.Constants.ACCEPTABLE_TURRET_ERROR;
import static frc.robot.Constants.ACCEPTABLE_TURRET_VF;

public class ResolveTarget extends CommandBase {
  private final Turret turret;
  private final double currentHeading;
  private final double offset;

  public ResolveTarget(Turret turret, DoubleSupplier currentHeading, DoubleSupplier offset) {
    this.turret = turret;
    this.currentHeading = currentHeading.getAsDouble();
    this.offset = offset.getAsDouble();
    addRequirements(turret);
  }

  @Override
  public void initialize() {
    NetworkTableInstance.getDefault().getTable("limelight").getEntry("pipeline").setNumber(1);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    turret.setTurretControlled(currentHeading + offset);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    NetworkTableInstance.getDefault().getTable("limelight").getEntry("pipeline").setNumber(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return Math.abs(offset) < ACCEPTABLE_TURRET_ERROR
            && Math.abs(turret.getCurrentVelocity()) < ACCEPTABLE_TURRET_VF;
  }
}
