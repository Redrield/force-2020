
package frc.robot.commands;

import frc.robot.subsystems.Intake;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.CommandBase;

public class ActuateIntakePistons extends CommandBase {
  private final Intake intake;
  private final Value position;
  
  public ActuateIntakePistons(Intake intake, Value position) {
    this.intake = intake;
    this.position = position;
    addRequirements(intake);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    intake.setPistons(position);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
