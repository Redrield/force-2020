
package frc.robot.commands;

import frc.robot.subsystems.Shooter;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.CommandBase;

public class ActuateShooterMotor extends CommandBase {
  private final Shooter shooter;
  private final double speed;
  private final NetworkTableEntry speedEntry;

  public ActuateShooterMotor(Shooter shooter, double speed) {
    this.shooter = shooter;
    this.speed = speed;
    this.speedEntry = NetworkTableInstance.getDefault().getTable("shooter").getEntry("speed");
    addRequirements(shooter);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    shooter.setShooterRaw(speedEntry.getDouble(speed));
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    shooter.setShooterRaw(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
