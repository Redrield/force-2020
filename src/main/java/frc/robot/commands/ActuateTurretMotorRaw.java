
package frc.robot.commands;

import frc.robot.subsystems.Turret;

import edu.wpi.first.wpilibj2.command.CommandBase;

public class ActuateTurretMotorRaw extends CommandBase {
  private final Turret turret;
  private final double speed;

  public ActuateTurretMotorRaw(Turret turret, double speed) {
    this.turret = turret;
    this.speed = speed;
    addRequirements(turret);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    turret.setTurretRaw(speed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    turret.setTurretRaw(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
