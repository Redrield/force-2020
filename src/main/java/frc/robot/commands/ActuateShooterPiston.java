
package frc.robot.commands;

import frc.robot.subsystems.Shooter;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.CommandBase;

public class ActuateShooterPiston extends CommandBase {
  private final Shooter shooter;
  private final Value position;

  public ActuateShooterPiston(Shooter shooter, Value position) {
    this.shooter = shooter;
    this.position = position;
    addRequirements(shooter);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    shooter.setPiston(position);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
