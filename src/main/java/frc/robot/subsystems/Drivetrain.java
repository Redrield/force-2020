/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

import static frc.robot.Constants.*;

public class Drivetrain extends SubsystemBase {
  private final WPI_TalonFX leftMaster = new WPI_TalonFX(DRIVETRAIN_LEFT_MASTER_PORT);
  private final WPI_TalonFX leftSlave = new WPI_TalonFX(DRIVETRAIN_LEFT_SLAVE_PORT);
  private final WPI_TalonFX rightMaster = new WPI_TalonFX(DRIVETRAIN_RIGHT_MASTER_PORT);
  private final WPI_TalonFX rightSlave = new WPI_TalonFX(DRIVETRAIN_RIGHT_SLAVE_PORT);

  private final SpeedControllerGroup leftGroup = new SpeedControllerGroup(leftMaster, leftSlave);
  private final SpeedControllerGroup rightGroup = new SpeedControllerGroup(rightMaster, rightSlave);
    
  private final DifferentialDrive drive = new DifferentialDrive(leftGroup, rightGroup);

  public void arcadeDrive(double speed, double rotation) {
    drive.arcadeDrive(-1*speed, rotation, true);
  }

}
