/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import static frc.robot.Constants.*;

public class Intake extends SubsystemBase {
  private final DoubleSolenoid pistons = new DoubleSolenoid(
      INTAKE_PCM_PORT_FORWARD, 
      INTAKE_PCM_PORT_REVERSE
  );
  private final WPI_VictorSPX wheelMotor = new WPI_VictorSPX(INTAKE_MOTOR_PORT);

  public void setIntakeRaw(double speed) {
    wheelMotor.set(ControlMode.PercentOutput, speed);
  }

  public void setPistons(Value position) {
    pistons.set(position);
  }

}
