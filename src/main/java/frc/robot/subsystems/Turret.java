/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import static frc.robot.Constants.*;

public class Turret extends SubsystemBase {
  private final WPI_TalonSRX turretMotor = new WPI_TalonSRX(SHOOTER_TURRET_PORT);

  private static final double KP = 0;
  private static final double KI = 0;
  private static final double KD = 0;
  private static final double KF = 0;

  private static final double SMALL_SPROCKET_OUTER_RADIUS = 0.572;
  private static final double BIG_SPROCKET_OUTER_RADIUS = 4.708;

  private static final double FULL_CIRCLE = 360;
  private static final double TICKS_PER_ROTATION = 4096;

  private static final int NEG_MAX = -30;
  private static final int POS_MAX = 30;

  public Turret() {
    turretMotor.setSensorPhase(true);
    turretMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 10);
    turretMotor.setNeutralMode(NeutralMode.Brake);
    turretMotor.config_kP(0, KP);
    turretMotor.config_kF(0, KF);
    turretMotor.config_kI(0, KI);
    turretMotor.config_kD(0, KD);
    turretMotor.setSelectedSensorPosition(0);
  }

  private boolean legalTurret(double degree) {
    return degree > NEG_MAX && degree < POS_MAX;
  }

  public double getCurrentHeading() {
    return ticksToDegrees(turretMotor.getSelectedSensorPosition());
  }

  public double getCurrentVelocity() {
    return ticksToDegrees(turretMotor.getSelectedSensorVelocity()) * 10;
  }

  private double ticksToDegrees(int ticks) {
    double sprocketRatio = SMALL_SPROCKET_OUTER_RADIUS / BIG_SPROCKET_OUTER_RADIUS;
    return (((ticks / TICKS_PER_ROTATION) * sprocketRatio * FULL_CIRCLE)) % FULL_CIRCLE;
  }

  private double degreesToTicks(double degrees) {
    double sprocketRatio = BIG_SPROCKET_OUTER_RADIUS / SMALL_SPROCKET_OUTER_RADIUS;
    return Math.floor((degrees / 360) * sprocketRatio * TICKS_PER_ROTATION);
  }

  // Be careful
  public void setTurretRaw(double speed) {
    turretMotor.set(ControlMode.PercentOutput, speed);
  }

  public void setTurretControlled(double degree) {
    if (legalTurret(degree)) {
      turretMotor.set(ControlMode.Position, degreesToTicks(degree));
    }
  }
}
