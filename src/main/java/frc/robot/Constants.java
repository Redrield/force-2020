/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants.  This class should not be used for any other purpose.  All constants should be
 * declared globally (i.e. public static).  Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

  public static final int SHOOTER_MASTER_PORT = 22;
  public static final int SHOOTER_SLAVE_PORT = 20;

  public static final int SHOOTER_TURRET_PORT = 19;

  public static final int HOPPER_MOTOR_PORT = 25;

  public static final int TOWER_MOTOR_PORT = 23;

  public static final int INTAKE_MOTOR_PORT = 24;

  public static final int DRIVETRAIN_LEFT_MASTER_PORT = 5;
  public static final int DRIVETRAIN_LEFT_SLAVE_PORT = 7;
  public static final int DRIVETRAIN_RIGHT_MASTER_PORT = 6;
  public static final int DRIVETRAIN_RIGHT_SLAVE_PORT = 8;

  public static final int SHOOTER_PCM_PORT_FORWARD = 0;
  public static final int SHOOTER_PCM_PORT_REVERSE = 1;

  public static final int INTAKE_PCM_PORT_FORWARD = 2;
  public static final int INTAKE_PCM_PORT_REVERSE = 3;

  public static final int FALCON_COOLER_ONE = 4;
  public static final int FALCON_COOLER_TWO = 5;  

  public static final double ACCEPTABLE_TURRET_ERROR = 2;
  public static final double ACCEPTABLE_TURRET_VF = 1.5;

  public static final int SPARK_MAX_LEAD_PORT = 17;
  public static final int SPARK_MAX_FOLLOW_PORT = 18;
}
