package frc.robot.olibs.util;

import edu.wpi.first.wpilibj.XboxController;

public class DualActionController extends XboxController {
  public DualActionController(int port) {
    super(port);
  }

  public boolean getRawButtonReleased(DualAction button) {
    return super.getRawButtonReleased(button.getValue());
  }

  public boolean getRawButtonPressed(DualAction button) {
    return super.getRawButtonPressed(button.getValue());
  }

  public boolean getRawButton(DualAction button) {
    return super.getRawButton(button.getValue());
  }

  public double getRawAxis(DualAction stick) {
    return super.getRawAxis(stick.getValue());
  }
}
