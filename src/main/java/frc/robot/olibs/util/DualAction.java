package frc.robot.olibs.util;

public enum DualAction {
  LEFT_X_AXIS(0),
  LEFT_Y_AXIS(1),
  RIGHT_X_AXIS(2),
  RIGHT_Y_AXIS(3),
  DPAD_X_AXIS(4),
  DPAD_Y_AXIS(5),
  X(1),
  A(2),
  B(3),
  Y(4),
  LEFT_BUMPER(5),
  RIGHT_BUMPER(6),
  LEFT_TRIGGER(7),
  RIGHT_TRIGGER(8),
  BACK_BUTTON(9),
  START_BUTTON(10),
  LEFT_STICK_BUTTON(11),
  RIGHT_STICK_BUTTON(12);

  private int value;

  DualAction(int value) {
    this.value = value;
  }

  public int getValue() {
    return this.value;
  }
}
