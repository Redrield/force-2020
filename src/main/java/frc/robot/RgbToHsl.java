package frc.robot;

public class RgbToHsl {
  // Rev Robotics color sensor V3 datasheet:
  // https://www.broadcom.com/products/optical-sensors/integrated-ambient-light-and-proximity-sensors/apds-9151

  /**
   * Converts an array of [R, G, B] values to HSL and copies the result
   * into a preallocated array.
   *
   * @param rgb    the RGB array. Must have length 3
   * @param hsl    the HSL array. Must have length 3
   * @param maxRgb the maximum value of the RGB channels (typically 255 for 8-bit color)
   * @return the HSL array
   */
  public static double[] convert(double[] rgb, double[] hsl, double maxRgb) {
    var r = rgb[0];
    var g = rgb[1];
    var b = rgb[2];

    var rr = r / maxRgb;
    var gg = g / maxRgb;
    var bb = b / maxRgb;

    var cmax = Math.max(rr, Math.max(gg, bb)); // max(rr, gg, bb)
    var cmin = Math.min(rr, Math.min(gg, bb)); // min(rr, gg, bb)
    var delta = cmax - cmin;

    double hue = calculateHue(rr, gg, bb);
    double luminance = calculateLuminance(cmax, cmin);
    double saturation = calculateSaturation(delta, luminance);

    hsl[0] = hue;
    hsl[1] = saturation;
    hsl[2] = luminance;

    return hsl;
  }

  /**
   * Calculates the hue value.
   *
   * @param rr the normalized red channel value in the range [0, 1]
   * @param gg the normalized green channel value in the range [0, 1]
   * @param bb the normalized blue channel value in the range [0, 1]
   * @return the calculated hue
   */
  private static double calculateHue(double rr, double gg, double bb) {
    var cmax = Math.max(rr, Math.max(gg, bb)); // max(rr, gg, bb)
    var cmin = Math.min(rr, Math.min(gg, bb)); // min(rr, gg, bb)
    var delta = cmax - cmin;

    double hue;

    if (delta == 0) {
      return 0;
    } else if (cmax == rr) {
      hue = 60 * ((gg - bb) / delta) % 6;
    } else if (cmax == gg) {
      hue = 60 * ((bb - rr) / delta + 2);
    } else {
      // cmax == bb
      hue = 60 * ((rr - gg) / delta + 4);
    }

    if (hue < 0) {
      hue += 360;
    }
    return hue;
  }

  /**
   * Calculates the saturation value.
   *
   * @param delta     the difference between the largest and smallest normalized channel values
   * @param luminance the calculated luminance value
   * @return the calculated saturation
   */
  private static double calculateSaturation(double delta, double luminance) {
    double saturation;

    if (delta == 0) {
      saturation = 0;
    } else {
      saturation = delta / (1 - Math.abs(2 * luminance - 1));
    }
    return saturation;
  }

  /**
   * Calculates the luminance value.
   *
   * @param cmax the maximum normalized channel value
   * @param cmin the minimum normalized channel value
   * @return the calculated luminance
   */
  private static double calculateLuminance(double cmax, double cmin) {
    return (cmax + cmin) / 2;
  }
}
