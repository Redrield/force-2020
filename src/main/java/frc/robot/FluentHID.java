package frc.robot;

import frc.robot.olibs.util.DualAction;
import frc.robot.olibs.util.DualActionController;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;

import java.util.HashMap;
import java.util.Map;

/**
 * An implementation of a Human Interface Device (HID) that wraps another to make it easier to use
 * in the command-based framework.
 *
 * Usage:
 *
 * FluentHID driverController = new FluentHID(new XboxController(Constants.DRIVER_CONTROLLER_PORT));
 *
 * driverController.button(XboxController.Button.kX).whileHeld(new ExampleCommand());
 */
public class FluentHID extends DualActionController {

  private final DualActionController hid;
  private final Map<Integer, JoystickButton> buttons = new HashMap<>();

  public FluentHID(DualActionController hid) {
    super(hid.getPort());

    this.hid = hid;
  }

  public JoystickButton button(int buttonNumber) {
    if (buttons.containsKey(buttonNumber)) {
      return buttons.get(buttonNumber);
    } else {
      JoystickButton button = new JoystickButton(this, buttonNumber);
      buttons.put(buttonNumber, button);
      return button;
    }

    // Can also be written as:
    // return buttons.computeIfAbsent(buttonNumber, (port) -> new JoystickButton(this, port));
  }

  public JoystickButton button(XboxController.Button button) {
    return button(button.value);
  }

  public JoystickButton button(DualAction dualActionButton) {
    return button(dualActionButton.getValue());
  }

  @Override
  public boolean getRawButton(int button) {
    return hid.getRawButton(button);
  }

  @Override
  public boolean getRawButtonPressed(int button) {
    return hid.getRawButtonPressed(button);
  }

  @Override
  public boolean getRawButtonReleased(int button) {
    return hid.getRawButtonReleased(button);
  }
  
  @Override
  public double getRawAxis(DualAction axis) {
    return hid.getRawAxis(axis);
  }

  @Override
  public int getPOV(int pov) {
    return hid.getPOV(pov);
  }

  @Override
  public int getPOV() {
    return hid.getPOV();
  }

  @Override
  public int getAxisCount() {
    return hid.getAxisCount();
  }

  @Override
  public int getPOVCount() {
    return hid.getPOVCount();
  }

  @Override
  public int getButtonCount() {
    return hid.getButtonCount();
  }

  @Override
  public HIDType getType() {
    return hid.getType();
  }

  @Override
  public String getName() {
    return hid.getName();
  }

  @Override
  public int getAxisType(int axis) {
    return hid.getAxisType(axis);
  }

  @Override
  public int getPort() {
    return hid.getPort();
  }

  @Override
  public void setOutput(int outputNumber, boolean value) {
    hid.setOutput(outputNumber, value);
  }

  @Override
  public void setOutputs(int value) {
    hid.setOutputs(value);
  }

  @Override
  public void setRumble(RumbleType type, double value) {
    hid.setRumble(type, value);
  }

  @Override
  public double getX(Hand hand) {
    return hid.getX(hand);
  }

  @Override
  public double getY(Hand hand) {
    return hid.getY(hand);
  }
}
